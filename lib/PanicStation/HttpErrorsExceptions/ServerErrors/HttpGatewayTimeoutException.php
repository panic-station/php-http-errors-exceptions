<?php


namespace PanicStation\HttpErrorsExceptions\ServerErrors;



use
    Exception;

/**
 * Represents 504 Gateway Timeout HTTP error
 *
 * The server, while acting as a gateway or proxy, did not receive a timely
 * response from the upstream server specified by the URI (e.g. HTTP, FTP, LDAP)
 * or some other auxiliary server (e.g. DNS) it needed to access in attempting
 * to complete the request.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.5.5
 *
 * @package PanicStation\HttpErrorsExceptions\ServerErrors
 */
class HttpGatewayTimeoutException extends Exception implements IHttpServerErrorException
{

    public function __construct(
        $message = "Gateway Timeout",
        $code = 504,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}