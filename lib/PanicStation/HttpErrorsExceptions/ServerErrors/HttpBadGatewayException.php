<?php


namespace PanicStation\HttpErrorsExceptions\ServerErrors;



use
    Exception;

/**
 * Represents 502 Bad Gateway HTTP error
 *
 * The server, while acting as a gateway or proxy, received an invalid response
 * from the upstream server it accessed in attempting to fulfill the request.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.5.3
 *
 * @package PanicStation\HttpErrorsExceptions\ServerErrors
 */
class HttpBadGatewayException extends Exception implements IHttpServerErrorException
{

    public function __construct(
        $message = "Bad Gateway",
        $code = 502,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 