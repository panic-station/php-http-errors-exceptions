<?php


namespace PanicStation\HttpErrorsExceptions\ServerErrors;



use
    Exception;

/**
 * Represents 511 Network Authentication Required HTTP error
 *
 * The 511 status code indicates that the client needs to authenticate to gain
 * network access.
 *
 * @link http://tools.ietf.org/html/rfc6585#section-6
 *
 * @package PanicStation\HttpErrorsExceptions\ServerErrors
 */
class HttpNetworkAuthenticationRequiredException extends Exception implements IHttpServerErrorException
{

    public function __construct(
        $message = "Network Authentication Required",
        $code = 511,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}