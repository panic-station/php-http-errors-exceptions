<?php


namespace PanicStation\HttpErrorsExceptions\ServerErrors;



use
    Exception;

/**
 * Represents 500 Internal Server Error HTTP error
 *
 * The server encountered an unexpected condition which prevented it from
 * fulfilling the request.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.5.1
 *
 * @package PanicStation\HttpErrorsExceptions\ServerErrors
 */
class HttpInternalServerErrorException extends Exception implements IHttpServerErrorException
{

    public function __construct(
        $message = "Internal Server Error",
        $code = 500,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 