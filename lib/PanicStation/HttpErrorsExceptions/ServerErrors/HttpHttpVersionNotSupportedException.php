<?php


namespace PanicStation\HttpErrorsExceptions\ServerErrors;



use
    Exception;

/**
 * Represents 505 HTTP Version Not Supported HTTP error
 *
 * The server does not support, or refuses to support, the HTTP protocol version
 * that was used in the request message.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.5.6
 *
 * @package PanicStation\HttpErrorsExceptions\ServerErrors
 */
class HttpHttpVersionNotSupportedException extends Exception implements IHttpServerErrorException
{

    public function __construct(
        $message = "HTTP Version Not Supported",
        $code = 505,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}