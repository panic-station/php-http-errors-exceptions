<?php


namespace PanicStation\HttpErrorsExceptions\ServerErrors;



use
    Exception;

/**
 * Represents 503 Service Unavailable HTTP error
 *
 * The server is currently unable to handle the request due to a temporary
 * overloading or maintenance of the server.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.5.4
 *
 * @package PanicStation\HttpErrorsExceptions\ServerErrors
 */
class HttpServiceUnavailableException extends Exception implements IHttpServerErrorException
{

    public function __construct(
        $message = "Service Unavailable",
        $code = 503,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}