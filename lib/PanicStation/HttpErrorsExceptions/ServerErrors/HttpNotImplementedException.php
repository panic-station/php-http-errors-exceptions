<?php


namespace PanicStation\HttpErrorsExceptions\ServerErrors;



use
    Exception;

/**
 * Represents 501 Not Implemented HTTP error
 *
 * The server does not support the functionality required to fulfill the
 * request.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.5.2
 *
 * @package PanicStation\HttpErrorsExceptions\ServerErrors
 */
class HttpNotImplementedException extends Exception implements IHttpServerErrorException
{

    public function __construct(
        $message = "Not Implemented",
        $code = 501,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 