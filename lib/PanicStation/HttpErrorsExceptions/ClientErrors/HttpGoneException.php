<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 410 Gone HTTP error
 *
 * The requested resource is no longer available at the server and no forwarding
 * address is known. This condition is expected to be considered permanent.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.11
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpGoneException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Gone",
        $code = 410,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 