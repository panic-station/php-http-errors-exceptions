<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 416 Requested Range Not Satisfiable HTTP error
 *
 * The client has asked for a portion of the file, but the server cannot supply
 * that portion.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.17
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpRequestedRangeNotSatisfiableException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Requested Range Not Satisfiable",
        $code = 416,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 