<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 413 Request Entity Too Large HTTP error
 *
 * The server is refusing to process a request because the request entity is
 * larger than the server is willing or able to process.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.14
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpRequestEntityTooLargeException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Request Entity Too Large",
        $code = 413,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 