<?php

namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 431 Request Header Fields Too Large HTTP error
 *
 * The 431 status code indicates that the server is unwilling to process the
 * request because its header fields are too large.
 *
 * @link http://tools.ietf.org/html/rfc6585#section-5
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpRequestHeaderFieldsTooLargeException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Request Header Fields Too Large",
        $code = 431,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }

}