<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 404 Not Found HTTP error
 *
 * The server has not found anything matching the Request-URI.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.5
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpNotFoundException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Not Found",
        $code = 404,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 