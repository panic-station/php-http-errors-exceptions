<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 406 Not Acceptable HTTP error
 *
 * The resource identified by the request is only capable of generating response
 * entities which have content characteristics not acceptable according to the
 * accept headers sent in the request.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.7
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpNotAcceptableException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Not Acceptable",
        $code = 406,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 