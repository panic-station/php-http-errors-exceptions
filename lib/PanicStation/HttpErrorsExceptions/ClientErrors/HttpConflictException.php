<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 409 Conflict HTTP error
 *
 * The request could not be completed due to a conflict with the current state
 * of the resource. This code is only allowed in situations where it is expected
 * that the user might be able to resolve the conflict and resubmit the request.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.10
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpConflictException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Conflict",
        $code = 409,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 