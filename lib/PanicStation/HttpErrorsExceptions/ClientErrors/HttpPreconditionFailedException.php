<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 412 Precondition Failed HTTP error
 *
 * The precondition given in one or more of the request-header fields evaluated
 * to false when it was tested on the server.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.13
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpPreconditionFailedException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Precondition Failed",
        $code = 412,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 