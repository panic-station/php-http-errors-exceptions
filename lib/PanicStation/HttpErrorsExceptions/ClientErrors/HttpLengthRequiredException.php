<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 411 Length Required HTTP error
 *
 * The server refuses to accept the request without a defined Content-Length.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.12
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpLengthRequiredException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Length Required",
        $code = 411,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 