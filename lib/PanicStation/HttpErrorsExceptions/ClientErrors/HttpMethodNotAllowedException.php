<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 405 Method Not Allowed HTTP error
 *
 * The method specified in the Request-Line is not allowed for the resource
 * identified by the Request-URI.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.6
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpMethodNotAllowedException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Method Not Allowed",
        $code = 405,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 