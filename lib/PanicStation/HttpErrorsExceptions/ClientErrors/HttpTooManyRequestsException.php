<?php

namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 429 Too Many Requests HTTP error
 *
 * The 429 status code indicates that the user has sent too many requests in a
 * given amount of time ("rate limiting").
 *
 * @link http://tools.ietf.org/html/rfc6585#section-4
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpTooManyRequestsException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Too Many Requests",
        $code = 429,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }

}