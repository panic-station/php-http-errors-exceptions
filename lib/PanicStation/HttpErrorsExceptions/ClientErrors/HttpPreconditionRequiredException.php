<?php

namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 428 Precondition Required HTTP error
 *
 * The 428 status code indicates that the origin server requires the request to
 * be conditional.
 *
 * @link http://tools.ietf.org/html/rfc6585#section-3
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpPreconditionRequiredException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Precondition Required",
        $code = 428,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }

}
 