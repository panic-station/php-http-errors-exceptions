<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 414 Request-URI Too Long HTTP error
 *
 * The server is refusing to service the request because the Request-URI is
 * longer than the server is willing to interpret.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.15
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpRequestUriTooLongException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Request-URI Too Long",
        $code = 414,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 