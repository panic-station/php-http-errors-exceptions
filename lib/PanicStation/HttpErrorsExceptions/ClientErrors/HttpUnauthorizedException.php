<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents the "401 Unauthorized" HTTP error
 *
 * Similar to 403 Forbidden, but specifically for use when authentication is
 * required and has failed or has not yet been provided. The response must
 * include a WWW-Authenticate header field containing a challenge applicable to
 * the requested resource.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.2
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpUnauthorizedException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Unauthorized",
        $code = 401,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 