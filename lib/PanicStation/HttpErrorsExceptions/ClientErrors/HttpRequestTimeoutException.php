<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 408 Request Timeout HTTP error
 *
 * The client did not produce a request within the time that the server was
 * prepared to wait.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.9
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpRequestTimeoutException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Request Timeout",
        $code = 408,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 