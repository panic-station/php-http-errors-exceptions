<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 402 Payment Required HTTP error
 *
 * This code is reserved for future use.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.3
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpPaymentRequiredException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Payment Required",
        $code = 402,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 