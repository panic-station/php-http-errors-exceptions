<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 417 Expectation Failed HTTP error
 *
 * The server cannot meet the requirements of the Expect request-header field.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.18
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpExpectationFailedException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Expectation Failed",
        $code = 417,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 