<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 407 Proxy Authentication Required HTTP error
 *
 * This code is similar to 401 (Unauthorized), but indicates that the client
 * must first authenticate itself with the proxy.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.8
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpProxyAuthenticationRequiredException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Proxy Authentication Required",
        $code = 407,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 