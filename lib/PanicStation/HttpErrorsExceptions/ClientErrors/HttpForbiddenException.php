<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 403 Forbidden HTTP error
 *
 * The request was a valid request, but the server is refusing to respond to it.
 * Authorization will not help and the request SHOULD NOT be repeated.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.4
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpForbiddenException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Forbidden",
        $code = 403,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 