<?php

namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents the "400 Bad Request" HTTP error
 *
 * The request could not be understood by the server due to malformed syntax.
 * The client SHOULD NOT repeat the request without modifications.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.1
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpBadRequestException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Bad Request",
        $code = 400,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }

} 