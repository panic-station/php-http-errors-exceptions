<?php


namespace PanicStation\HttpErrorsExceptions\ClientErrors;



use
    Exception;

/**
 * Represents 415 Unsupported Media Type HTTP error
 *
 * The server is refusing to service the request because the entity of the
 * request is in a format not supported by the requested resource for the
 * requested method.
 *
 * @link http://tools.ietf.org/html/rfc2616#section-10.4.16
 *
 * @package PanicStation\HttpErrorsExceptions\ClientErrors
 */
class HttpUnsupportedMediaTypeException extends Exception implements IHttpClientErrorException
{

    public function __construct(
        $message = "Unsupported Media Type",
        $code = 415,
        Exception $previous = null
    ) {

        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
} 