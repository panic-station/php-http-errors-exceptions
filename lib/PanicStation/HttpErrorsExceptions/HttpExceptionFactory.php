<?php


namespace PanicStation\HttpErrorsExceptions;



/**
 * Class HttpExceptionFactory
 *
 *
 * @package PanicStation\HttpErrorsExceptions
 */
class HttpExceptionFactory
{

    /**
     * Namespace that contains client error exceptions
     */
    const CLIENT_ERRORS_NAMESPACE = 'ClientErrors';

    /**
     * Namespace that contains server error exceptions
     */
    const SERVER_ERRORS_NAMESPACE = 'ServerErrors';

    /**
     * Exceptions classes prefix
     */
    const CLASS_PREFIX = 'Http';

    /**
     * Exceptions classes suffix
     */
    const CLASS_SUFFIX = 'Exception';

    /**
     * Contains all supported client errors codes and respective class names
     *
     * @var array
     */
    protected static $clientErrorsCodes = Array(

        // RFC2616 client errors
        400 => 'BadRequest',
        401 => 'Unauthorized',
        402 => 'PaymentRequired',
        403 => 'Forbidden',
        404 => 'NotFound',
        405 => 'MethodNotAllowed',
        406 => 'NotAcceptable',
        407 => 'ProxyAuthenticationRequired',
        408 => 'RequestTimeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'LengthRequired',
        412 => 'PreconditionFailed',
        413 => 'RequestEntityTooLarge',
        414 => 'RequestUriTooLong',
        415 => 'UnsupportedMediaType',
        416 => 'RequestedRangeNotSatisfiable',
        417 => 'ExpectationFailed',

        // RFC6585 client errors
        428 => 'PreconditionRequired',
        429 => 'TooManyRequests',
        431 => 'RequestHeaderFieldsTooLarge',
    );

    /**
     * Contains all supported server errors codes and respective class names
     *
     * @var array
     */
    protected static $serverErrorsCodes = Array(

        // RFC2616 server errors
        500 => 'InternalServerError',
        501 => 'NotImplemented',
        502 => 'BadGateway',
        503 => 'ServiceUnavailable',
        504 => 'GatewayTimeout',
        505 => 'HttpVersionNotSupported',

        // RFC6585 server errors
        511 => 'NetworkAuthenticationRequired',
    );


    /**
     * Instantiates and exception class based on given HTTP error code.
     *
     * @param int $code An HTTP error code to create an Exception for
     *
     * @return IHttpErrorException Appropriate Exception instance for the given error code
     *
     * @throws \InvalidArgumentException
     */
    public static function createException( $code )
    {

        if ( !self::isErrorCode( $code ) ) {

            throw new \InvalidArgumentException(
                __METHOD__
                .' accepts only one of valid HTTP error codes: '
                .implode(
                    ',',
                    array_merge(
                        array_keys( self::$clientErrorsCodes ),
                        array_keys( self::$serverErrorsCodes )
                    )
                )
            );
        }

        if ( isset( self::$clientErrorsCodes[$code] ) )
        {
            $namespace = self::CLIENT_ERRORS_NAMESPACE;
            $className = self::$clientErrorsCodes[$code];
        }
        else
        {
            $namespace = self::SERVER_ERRORS_NAMESPACE;
            $className = self::$serverErrorsCodes[$code];
        }

        $fullyQualifiedClassName =
            __NAMESPACE__
            .'\\'
            .$namespace
            .'\\'
            .self::CLASS_PREFIX
            .$className
            .self::CLASS_SUFFIX;

        return new $fullyQualifiedClassName();
    }


    /**
     * Checks if given code is HTTP error code.
     *
     * @param int $code Code to check
     *
     * @return bool True if given code is HTTP error code, false if it is not.
     */
    public static function isErrorCode( $code )
    {
        $result = false;

        if (
            isset( self::$clientErrorsCodes[$code] )
            || isset( self::$serverErrorsCodes[$code] )
        )
        {
            $result = true;
        }

        return $result;
    }
}