<?php


namespace PanicStation\HttpErrorsExceptions;



/**
 * Enables to catch ANY HTTP exception
 *
 * @package PanicStation\HttpErrorsExceptions
 */
interface IHttpErrorException
{

} 